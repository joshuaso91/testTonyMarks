<?php
namespace App\Test\TestCase\Controller;

use App\Controller\StocktakesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StocktakesController Test Case
 */
class StocktakesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stocktakes',
        'app.user_stores',
        'app.users',
        'app.stores',
        'app.price_checks',
        'app.suppliers',
        'app.user_store_orders',
        'app.products',
        'app.invoices',
        'app.user_store_suppliers'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
