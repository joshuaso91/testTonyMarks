<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserStoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserStoresTable Test Case
 */
class UserStoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserStoresTable
     */
    public $UserStores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_stores',
        'app.users',
        'app.stores',
        'app.price_checks',
        'app.suppliers',
        'app.user_store_orders',
        'app.products',
        'app.stocktakes',
        'app.invoices',
        'app.user_store_suppliers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserStores') ? [] : ['className' => 'App\Model\Table\UserStoresTable'];
        $this->UserStores = TableRegistry::get('UserStores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserStores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
