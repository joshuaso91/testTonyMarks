<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserStoreSuppliersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserStoreSuppliersTable Test Case
 */
class UserStoreSuppliersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserStoreSuppliersTable
     */
    public $UserStoreSuppliers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_store_suppliers',
        'app.user_stores',
        'app.users',
        'app.stores',
        'app.price_checks',
        'app.suppliers',
        'app.user_store_orders',
        'app.products',
        'app.stocktakes',
        'app.invoices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserStoreSuppliers') ? [] : ['className' => 'App\Model\Table\UserStoreSuppliersTable'];
        $this->UserStoreSuppliers = TableRegistry::get('UserStoreSuppliers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserStoreSuppliers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
