<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserStoreOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserStoreOrdersTable Test Case
 */
class UserStoreOrdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserStoreOrdersTable
     */
    public $UserStoreOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_store_orders',
        'app.user_stores',
        'app.users',
        'app.stores',
        'app.price_checks',
        'app.suppliers',
        'app.user_store_suppliers',
        'app.products',
        'app.stocktakes',
        'app.invoices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserStoreOrders') ? [] : ['className' => 'App\Model\Table\UserStoreOrdersTable'];
        $this->UserStoreOrders = TableRegistry::get('UserStoreOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserStoreOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
