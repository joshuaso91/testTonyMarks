<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserStoreOrder Entity
 *
 * @property int $id
 * @property int $user_store_id
 * @property int $supplier_id
 * @property int $product_id
 * @property float $ordered_qty
 * @property \Cake\I18n\Time $order_date
 * @property string $same_day_order
 * @property string $order_notes
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\UserStore $user_store
 * @property \App\Model\Entity\Supplier $supplier
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Invoice[] $invoices
 */
class UserStoreOrder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
