<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserStore Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $store_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Store $store
 * @property \App\Model\Entity\PriceCheck[] $price_checks
 * @property \App\Model\Entity\Stocktake[] $stocktakes
 * @property \App\Model\Entity\UserStoreOrder[] $user_store_orders
 * @property \App\Model\Entity\UserStoreSupplier[] $user_store_suppliers
 */
class UserStore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
	
	// virtual properties to get the UserStore User_name & Store_name
	protected function _getUserStoreNames()
	{
		// this return below returns the user_id and store_id of the table that's it's connected to.
		//return $this->user_id . ' - ' . $this->store_id;
		return $this->user->user_name . ' - ' . $this->store->store_name;
		//debug($this); die;
		
	}
}
