<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Stores Model
 *
 * @property \Cake\ORM\Association\HasMany $UserStores
 *
 * @method \App\Model\Entity\Store get($primaryKey, $options = [])
 * @method \App\Model\Entity\Store newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Store[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Store|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Store patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Store[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Store findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('stores');
		// setDisplayField - allows you to change the displayField to whatever you want to show to users
		// instead of the ID number for whatever tables that's connected to it. Initially it is
		// setDisplayField('id'); I changed it to setDisplayField('store_name'); to show users the name of the store when
		// the user wanna assign the stores to users or other forms.
        $this->setDisplayField('store_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
		
		// links to the Users table to get the data.
		$this->belongsToMany('Users',
							[
								'joinTable' => 'UserStores',
							]);
        $this->hasMany('UserStores', [
            'foreignKey' => 'store_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('store_name', 'create')
            ->notEmpty('store_name');

        $validator
            ->requirePresence('store_email', 'create')
            ->notEmpty('store_email');

        $validator
            ->requirePresence('store_phone_no', 'create')
            ->notEmpty('store_phone_no');

        $validator
            ->requirePresence('store_address', 'create')
            ->notEmpty('store_address');

        return $validator;
    }
}
