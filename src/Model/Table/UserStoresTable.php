<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserStores Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Stores
 * @property \Cake\ORM\Association\HasMany $PriceChecks
 * @property \Cake\ORM\Association\HasMany $Stocktakes
 * @property \Cake\ORM\Association\HasMany $UserStoreOrders
 * @property \Cake\ORM\Association\HasMany $UserStoreSuppliers
 *
 * @method \App\Model\Entity\UserStore get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserStore newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserStore[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserStore|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserStore patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserStore[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserStore findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserStoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_stores');
		$this->setDisplayField('user_store_names'); // the 'user_store_names' get inflected from the _getUserStoreNames in UserStores Entity.
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Stores', [
            'foreignKey' => 'store_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('PriceChecks', [
            'foreignKey' => 'user_store_id'
        ]);
        $this->hasMany('Stocktakes', [
            'foreignKey' => 'user_store_id'
        ]);
        $this->hasMany('UserStoreOrders', [
            'foreignKey' => 'user_store_id'
        ]);
        $this->hasMany('UserStoreSuppliers', [
            'foreignKey' => 'user_store_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['store_id'], 'Stores'));

        return $rules;
    }
    
    /* finding user_name and store_name to put through to index classes*/
    public function findUserStoreName(Query $query, array $options) // <<<- this is a finder query
{
    return $query->contain([
        'Users' => ['fields' => ['user_name']],
        'Stores' => ['fields' => ['store_name']]
    ]);
}
}
