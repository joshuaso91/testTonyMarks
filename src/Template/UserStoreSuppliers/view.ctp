<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Store Supplier'), ['action' => 'edit', $userStoreSupplier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Store Supplier'), ['action' => 'delete', $userStoreSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreSupplier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Store Suppliers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store Supplier'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userStoreSuppliers view large-9 medium-8 columns content">
    <h3><?= h($userStoreSupplier->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Store') ?></th>
            <td><?= $userStoreSupplier->has('user_store') ? $this->Html->link($userStoreSupplier->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $userStoreSupplier->user_store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier') ?></th>
            <td><?= $userStoreSupplier->has('supplier') ? $this->Html->link($userStoreSupplier->supplier->id, ['controller' => 'Suppliers', 'action' => 'view', $userStoreSupplier->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userStoreSupplier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userStoreSupplier->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userStoreSupplier->modified) ?></td>
        </tr>
    </table>
</div>
