<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Store Supplier'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userStoreSuppliers index large-9 medium-8 columns content">
    <h3><?= __('User Store Suppliers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_store_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userStoreSuppliers as $userStoreSupplier): ?>
            <tr>
                <td><?= $this->Number->format($userStoreSupplier->id) ?></td>
                <td><?= $userStoreSupplier->has('user_store') ? $this->Html->link($userStoreSupplier->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $userStoreSupplier->user_store->id]) : '' ?></td>
                <td><?= $userStoreSupplier->has('supplier') ? $this->Html->link($userStoreSupplier->supplier->supplier_name, ['controller' => 'Suppliers', 'action' => 'view', $userStoreSupplier->supplier->id]) : '' ?></td>
                <td><?= h($userStoreSupplier->created) ?></td>
                <td><?= h($userStoreSupplier->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userStoreSupplier->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userStoreSupplier->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userStoreSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreSupplier->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
