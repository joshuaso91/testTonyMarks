<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List User Store Suppliers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userStoreSuppliers form large-9 medium-8 columns content">
    <?= $this->Form->create($userStoreSupplier) ?>
    <fieldset>
        <legend><?= __('Add User Store Supplier') ?></legend>
        <?php
            echo $this->Form->control('user_store_id', ['options' => $userStores]);
            echo $this->Form->control('supplier_id', ['options' => $suppliers]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
