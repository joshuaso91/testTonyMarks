<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
		<h4>Other Navigations:-</h4>
		<?php if($superAdmin): ?>
        	<li><?= $this->Html->link(__('New Store'), ['action' => 'add']) ?></li>
        	<li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
		<?php endif; ?>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Stores') ?></h3>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('store_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('store_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('store_phone_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('store_address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($stores as $store): ?>
            <tr>
                <td><?= $this->Number->format($store->id) ?></td>
                <td><?= h($store->store_name) ?></td>
                <td><?= h($store->store_email) ?></td>
                <td><?= h($store->store_phone_no) ?></td>
                <td><?= h($store->store_address) ?></td>
                <td><?= h($store->created) ?></td>
                <td><?= h($store->modified) ?></td>
                <td class="actions">                    
                    <?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $store->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $store->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $store->id], ['confirm' => __('Are you sure you want to delete # {0}?', $store->id), 'class' => 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
