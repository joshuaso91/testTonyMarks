<?php
/**
  * @var \App\View\AppView $this
  */
?>

<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
        <h4>Other Navigations:-</h4>
		<?php if($superAdmin): ?>
        	<li><?= $this->Html->link(__('New User Store'), ['action' => 'add']) ?></li>
        	<li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        	<li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?></li>
        	<li><?= $this->Html->link(__('New Price Check'), ['controller' => 'PriceChecks', 'action' => 'add']) ?></li>
        	<li><?= $this->Html->link(__('New Stocktake'), ['controller' => 'Stocktakes', 'action' => 'add']) ?></li>
        	<li><?= $this->Html->link(__('New User Store Order'), ['controller' => 'UserStoreOrders', 'action' => 'add']) ?></li>
        	<li><?= $this->Html->link(__('New User Store Supplier'), ['controller' => 'UserStoreSuppliers', 'action' => 'add']) ?></li>
		<?php endif; ?>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Price Checks'), ['controller' => 'PriceChecks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Stocktakes'), ['controller' => 'Stocktakes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Store Orders'), ['controller' => 'UserStoreOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Store Suppliers'), ['controller' => 'UserStoreSuppliers', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('User Stores') ?></h3>
    <?php echo $this->Flash->render('message') ?>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('store_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <?php if($superAdmin): ?>
                	<th scope="col" class="actions"><?= __('Actions') ?></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userStores as $userStore): ?>
            <tr>
                <td><?= $this->Number->format($userStore->id) ?></td>
				<!-- The $this->Html->link($userStore->user->user_name) you can change the 'user_name' to anything else in the
				database table name to see the change of data in the index view. Initially it was link($userStore->user->id),
					I changed it to the latter because I want to show the name instead of the IDs number -->
                <td><?= $userStore->has('user') ? $this->Html->link($userStore->user->user_name, ['controller' => 'Users', 'action' => 'view', $userStore->user->id]) : '' ?></td>
                <td><?= $userStore->has('store') ? $this->Html->link($userStore->store->store_name, ['controller' => 'Stores', 'action' => 'view', $userStore->store->id]) : '' ?></td>
                <td><?= h($userStore->created) ?></td>
                <td><?= h($userStore->modified) ?></td>
				<?php if($superAdmin): ?>
                	<td class="actions">
                   		<?php /*
							$span_glyphiconView = $this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-eye-open'));
							echo $this->Html->link("View $span_glyphicon",array('controller' => 'UserStores', 'action' => 'view'),
							array('class' => 'push_menu navFont', 'escape' => false)) */
						?>
                   		<?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $userStore->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
                   		<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $userStore->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
                    	<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userStore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStore->id), 'class' => 'btn btn-danger']) ?>
                	</td>
				<?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
