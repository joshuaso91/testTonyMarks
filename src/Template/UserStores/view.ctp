<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Store'), ['action' => 'edit', $userStore->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Store'), ['action' => 'delete', $userStore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStore->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Stores'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Price Checks'), ['controller' => 'PriceChecks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Price Check'), ['controller' => 'PriceChecks', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stocktakes'), ['controller' => 'Stocktakes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Stocktake'), ['controller' => 'Stocktakes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Store Orders'), ['controller' => 'UserStoreOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store Order'), ['controller' => 'UserStoreOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Store Suppliers'), ['controller' => 'UserStoreSuppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store Supplier'), ['controller' => 'UserStoreSuppliers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userStores view large-9 medium-8 columns content">
    <h3><?= h($userStore->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $userStore->has('user') ? $this->Html->link($userStore->user->id, ['controller' => 'Users', 'action' => 'view', $userStore->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Store') ?></th>
            <td><?= $userStore->has('store') ? $this->Html->link($userStore->store->id, ['controller' => 'Stores', 'action' => 'view', $userStore->store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userStore->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userStore->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userStore->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Price Checks') ?></h4>
        <?php if (!empty($userStore->price_checks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Date Entered') ?></th>
                <th scope="col"><?= __('Product Price') ?></th>
                <th scope="col"><?= __('PriceCheck Notes') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userStore->price_checks as $priceChecks): ?>
            <tr>
                <td><?= h($priceChecks->id) ?></td>
                <td><?= h($priceChecks->user_store_id) ?></td>
                <td><?= h($priceChecks->supplier_id) ?></td>
                <td><?= h($priceChecks->product_id) ?></td>
                <td><?= h($priceChecks->date_entered) ?></td>
                <td><?= h($priceChecks->product_price) ?></td>
                <td><?= h($priceChecks->priceCheck_notes) ?></td>
                <td><?= h($priceChecks->created) ?></td>
                <td><?= h($priceChecks->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PriceChecks', 'action' => 'view', $priceChecks->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PriceChecks', 'action' => 'edit', $priceChecks->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PriceChecks', 'action' => 'delete', $priceChecks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $priceChecks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Stocktakes') ?></h4>
        <?php if (!empty($userStore->stocktakes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Current Stock Qty') ?></th>
                <th scope="col"><?= __('Suggested Order Qty') ?></th>
                <th scope="col"><?= __('Total Req Qty') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userStore->stocktakes as $stocktakes): ?>
            <tr>
                <td><?= h($stocktakes->id) ?></td>
                <td><?= h($stocktakes->user_store_id) ?></td>
                <td><?= h($stocktakes->product_id) ?></td>
                <td><?= h($stocktakes->current_stock_qty) ?></td>
                <td><?= h($stocktakes->suggested_order_qty) ?></td>
                <td><?= h($stocktakes->total_req_qty) ?></td>
                <td><?= h($stocktakes->created) ?></td>
                <td><?= h($stocktakes->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Stocktakes', 'action' => 'view', $stocktakes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Stocktakes', 'action' => 'edit', $stocktakes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stocktakes', 'action' => 'delete', $stocktakes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stocktakes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Store Orders') ?></h4>
        <?php if (!empty($userStore->user_store_orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Ordered Qty') ?></th>
                <th scope="col"><?= __('Order Date') ?></th>
                <th scope="col"><?= __('Same Day Order') ?></th>
                <th scope="col"><?= __('Order Notes') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userStore->user_store_orders as $userStoreOrders): ?>
            <tr>
                <td><?= h($userStoreOrders->id) ?></td>
                <td><?= h($userStoreOrders->user_store_id) ?></td>
                <td><?= h($userStoreOrders->supplier_id) ?></td>
                <td><?= h($userStoreOrders->product_id) ?></td>
                <td><?= h($userStoreOrders->ordered_qty) ?></td>
                <td><?= h($userStoreOrders->order_date) ?></td>
                <td><?= h($userStoreOrders->same_day_order) ?></td>
                <td><?= h($userStoreOrders->order_notes) ?></td>
                <td><?= h($userStoreOrders->created) ?></td>
                <td><?= h($userStoreOrders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserStoreOrders', 'action' => 'view', $userStoreOrders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserStoreOrders', 'action' => 'edit', $userStoreOrders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserStoreOrders', 'action' => 'delete', $userStoreOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreOrders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Store Suppliers') ?></h4>
        <?php if (!empty($userStore->user_store_suppliers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userStore->user_store_suppliers as $userStoreSuppliers): ?>
            <tr>
                <td><?= h($userStoreSuppliers->id) ?></td>
                <td><?= h($userStoreSuppliers->user_store_id) ?></td>
                <td><?= h($userStoreSuppliers->supplier_id) ?></td>
                <td><?= h($userStoreSuppliers->created) ?></td>
                <td><?= h($userStoreSuppliers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserStoreSuppliers', 'action' => 'view', $userStoreSuppliers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserStoreSuppliers', 'action' => 'edit', $userStoreSuppliers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserStoreSuppliers', 'action' => 'delete', $userStoreSuppliers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreSuppliers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
