<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="users form large-9 medium-8 columns content">
   <h1>Registration Page</h1>
    <?= $this->Form->create($user) ?>
    <fieldset>
        <?php
            echo $this->Form->control('user_name');
            echo $this->Form->control('user_email');
            echo $this->Form->control('password');
            if($superAdmin || $admin)
            {
                echo $this->Form->control('roles');
            }
            else
            {
                echo $this->Form->control('roles', array ('readonly' => 'readonly'));
            }
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<div>
	<?= $this->Html->link(__('Back'), ['controller' => 'Users', 'action' => 'login'], array('class' => 'btn btn-primary')) ?>
</div>