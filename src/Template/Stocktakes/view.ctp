<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Stocktake'), ['action' => 'edit', $stocktake->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Stocktake'), ['action' => 'delete', $stocktake->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stocktake->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Stocktakes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Stocktake'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="stocktakes view large-9 medium-8 columns content">
    <h3><?= h($stocktake->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Store') ?></th>
            <td><?= $stocktake->has('user_store') ? $this->Html->link($stocktake->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $stocktake->user_store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $stocktake->has('product') ? $this->Html->link($stocktake->product->id, ['controller' => 'Products', 'action' => 'view', $stocktake->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($stocktake->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Current Stock Qty') ?></th>
            <td><?= $this->Number->format($stocktake->current_stock_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Suggested Order Qty') ?></th>
            <td><?= $this->Number->format($stocktake->suggested_order_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Req Qty') ?></th>
            <td><?= $this->Number->format($stocktake->total_req_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($stocktake->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($stocktake->modified) ?></td>
        </tr>
    </table>
</div>
