<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Stocktakes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="stocktakes form large-9 medium-8 columns content">
    <?= $this->Form->create($stocktake) ?>
    <fieldset>
        <legend><?= __('Add Stocktake') ?></legend>
        <?php
            echo $this->Form->control('user_store_id', ['options' => $userStores]);
            echo $this->Form->control('product_id', ['type'=>'select','multiple'=>true,'options' => $products]);
            echo $this->Form->control('current_stock_qty');
            echo $this->Form->control('suggested_order_qty');
            echo $this->Form->control('total_req_qty');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
