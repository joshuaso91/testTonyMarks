<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
	<h4>Other Navigations:-</h4>
    <ul class="nav nav-pills">
        <li><?= $this->Html->link(__('New Price Check'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('Price Checks') ?></h3>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_store_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_entered') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($priceChecks as $priceCheck): ?>
            <tr>
                <td><?= $this->Number->format($priceCheck->id) ?></td>
                <td><?= $priceCheck->has('user_store') ? $this->Html->link($priceCheck->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $priceCheck->user_store->id]) : '' ?></td>
                <td><?= $priceCheck->has('supplier') ? $this->Html->link($priceCheck->supplier->supplier_name, ['controller' => 'Suppliers', 'action' => 'view', $priceCheck->supplier->id]) : '' ?></td>
                <td><?= $priceCheck->has('product') ? $this->Html->link($priceCheck->product->product_name, ['controller' => 'Products', 'action' => 'view', $priceCheck->product->id]) : '' ?></td>
                <td><?= h($priceCheck->date_entered) ?></td>
                <td><?= $this->Number->format($priceCheck->product_price) ?></td>
                <td><?= h($priceCheck->created) ?></td>
                <td><?= h($priceCheck->modified) ?></td>
                <td class="actions">
                    <?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $priceCheck->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $priceCheck->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $priceCheck->id], ['confirm' => __('Are you sure you want to delete # {0}?', $priceCheck->id), 'class' => 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
