<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Price Check'), ['action' => 'edit', $priceCheck->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Price Check'), ['action' => 'delete', $priceCheck->id], ['confirm' => __('Are you sure you want to delete # {0}?', $priceCheck->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Price Checks'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Price Check'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="priceChecks view large-9 medium-8 columns content">
    <h3><?= h($priceCheck->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Store') ?></th>
            <td><?= $priceCheck->has('user_store') ? $this->Html->link($priceCheck->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $priceCheck->user_store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier') ?></th>
            <td><?= $priceCheck->has('supplier') ? $this->Html->link($priceCheck->supplier->id, ['controller' => 'Suppliers', 'action' => 'view', $priceCheck->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $priceCheck->has('product') ? $this->Html->link($priceCheck->product->id, ['controller' => 'Products', 'action' => 'view', $priceCheck->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($priceCheck->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product Price') ?></th>
            <td><?= $this->Number->format($priceCheck->product_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Entered') ?></th>
            <td><?= h($priceCheck->date_entered) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($priceCheck->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($priceCheck->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('PriceCheck Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($priceCheck->priceCheck_notes)); ?>
    </div>
</div>
