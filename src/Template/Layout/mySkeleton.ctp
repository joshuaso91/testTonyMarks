<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('custom.css') ?> <!-- adding custom.css to the Cake FrameWork -->
    <!-- inserting jquery into the entire page -->
    <?= $this->Html->script('jquery.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <?php if($loggedIn): ?>
					<!-- *********************************************************** -->
					<!-- I've set and added the User loggedIn name and the Users role at the top of the navigation bar. 
						 I did it through the app controller -->
                    <li id="loggedInWelcome">Welcome, <?= $users_username; ?>:&nbsp</li>
                    <li id="loggedInWelcome"><?= $users_roles; ?></li>
                    <li><?= $this->Html->link('Logout', ['controller'=>'users', 'action'=>'logout']); ?></li>
                <?php else: ?>
                    <li><?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'add']); ?></li>
                    <li><?= $this->Html->link('Login', ['controller'=>'users', 'action'=>'login']); ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>

<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>


<!-- ************************************************************************************************************************************************************** -->



<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
	
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('tonymarks_custom.css') ?>
    <?= $this->Html->script('jquery.js') ?>
    <?= $this->Html->script('bootstrap.js') ?>
    <?= $this->Html->script('custom.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>
<body>
<nav class="navbar navbar-fixed-top background_Tony_Marks_Color">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header background_Tony_Marks_Color">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topFixedNavbar1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><?= $this->Html->image('logo_minified_2.png', ['class' => '']) ?></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="topFixedNavbar1">
        <ul class="nav navbar-nav">
        	<?php if($loggedIn): ?>
					<!-- *********************************************************** -->
					<!-- I've set and added the User loggedIn name and the Users role at the top of the navigation bar. 
						 I did it through the app controller -->
                    <li id="loggedInWelcome">Welcome, <?= $users_username; ?>:&nbsp</li>
                    <li id="loggedInWelcome"><?= $users_roles; ?></li>
                    <li><?= $this->Html->link('Logout', ['controller'=>'users', 'action'=>'logout']); ?></li>
                <?php else: ?>
                    <li><?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'add', array('class'=>'navFont font-bold')]); ?></li>
                    <li><?= $this->Html->link('Login', ['controller'=>'users', 'action'=>'login']); ?></li>
                <?php endif; ?>
        
			<li><a class="navFont font-bold" href="#">Link</a></li>
          <li><a href="html/game.html" class="dropdown-toggle navFont font-bold" data-toggle="dropdown" role="button">Link<span class="caret"></span></a>
          </li>
          <li class="dropdown"><a href="html/heroes.html" class="dropdown-toggle navFont font-bold" data-toggle="dropdown" role="button" aria-expanded="false">HEROES<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="html/heroes.html">Character Infomation</a></li>
              <li><a href="html/gameplay.html">Gameplay</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="html/screenshot.html" class="dropdown-toggle navFont font-bold" data-toggle="dropdown" role="button" aria-expanded="false">MEDIA<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="html/screenshot.html">Screen Shots</a></li>
              <li><a href="html/artwork.html">Artwork</a></li>
              <li><a href="html/video.html">Videos</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="html/heroes.html" class="dropdown-toggle navFont font-bold active" data-toggle="dropdown" role="button" aria-expanded="false">DEVELOPERS<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="html/developers.html">Developer's Infomation</a></li>
              <li><a href="html/developers-idea.html">Developer's Idea</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>




<?php  /***
	<?php // if($superAdmin || $admin || $staff): ?>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
    		<div id="navbar-top-left" class="navbar-header">
      			
      			<a href=""><?= $this->Html->image('logo.png') ?></a>
      			
    		</div>
    		<div class="navbar-header navbar-right">
     			<ul class="nav navbar-nav">
     				<?php if($loggedIn): ?>
					<!-- *********************************************************** -->
					<!-- I've set and added the User loggedIn name and the Users role at the top of the navigation bar. 
						 I did it through the app controller -->
                    <li id="loggedInWelcome">Welcome, <?= $users_username; ?>:&nbsp</li>
                    <li id="loggedInWelcome"><?= $users_roles; ?></li>
                    <li><?= $this->Html->link('Logout', ['controller'=>'users', 'action'=>'logout']); ?></li>
                <?php else: ?>
                    <li><?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'add']); ?></li>
                    <li><?= $this->Html->link('Login', ['controller'=>'users', 'action'=>'login']); ?></li>
                <?php endif; ?>
     			</ul>
      			
    		</div>
  		</div>
	</nav>
   <?php // endif; ?>
    **/
   ?>
    <?php /** <nav class="navbar navbar-default">
        <ul class="">
            <li class="">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <?php if($loggedIn): ?>
					<!-- *********************************************************** -->
					<!-- I've set and added the User loggedIn name and the Users role at the top of the navigation bar. 
						 I did it through the app controller -->
                    <li id="loggedInWelcome">Welcome, <?= $users_username; ?>:&nbsp</li>
                    <li id="loggedInWelcome"><?= $users_roles; ?></li>
                    <li><?= $this->Html->link('Logout', ['controller'=>'users', 'action'=>'logout']); ?></li>
                <?php else: ?>
                    <li><?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'add']); ?></li>
                    <li><?= $this->Html->link('Login', ['controller'=>'users', 'action'=>'login']); ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </nav> **/ ?>
    <?= $this->Flash->render() ?>
    
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>


<!-- ********************************************************************************** -->
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
	
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('tonymarks_custom.css') ?>
    <?= $this->Html->script('jquery.js') ?>
    <?= $this->Html->script('bootstrap.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>


</head>
<body>
<?php // if($superAdmin || $admin || $staff): ?>
<nav class="navbar navbar-fixed-top background_Tony_Marks_Color">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header background_Tony_Marks_Color">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topFixedNavbar1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><?= $this->Html->image('logo_minified_2.png', ['class' => '']) ?></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="topFixedNavbar1">
        <ul class="nav navbar-nav">
        	<?php if($loggedIn): ?>
					<!-- *********************************************************** -->
					<!-- I've set and added the User loggedIn name and the Users role at the top of the navigation bar. 
						 I did it through the app controller -->
                    <li id="loggedInWelcome">Welcome, <?= $users_username; ?>:&nbsp</li>
                    <li id="loggedInWelcome"><?= $users_roles; ?></li>
                    <li><?= $this->Html->link('Logout', ['controller'=>'users', 'action'=>'logout']); ?></li>
                <?php else: ?>
                    <li><?= $this->Html->link('Register', ['controller'=>'users', 'action'=>'add'], array('class' => 'navFont font-bold')); ?></li>
                    <li><?= $this->Html->link('Login', ['controller'=>'users', 'action'=>'login'], array('class' => 'navFont font-bold')); ?></li>
                <?php endif; ?>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
  </nav>

   <?php // endif; ?>

    <?= $this->Flash->render() ?>
    
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>



