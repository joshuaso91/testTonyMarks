<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Supplier'), ['action' => 'edit', $supplier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Supplier'), ['action' => 'delete', $supplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $supplier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Suppliers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supplier'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Price Checks'), ['controller' => 'PriceChecks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Price Check'), ['controller' => 'PriceChecks', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Store Orders'), ['controller' => 'UserStoreOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store Order'), ['controller' => 'UserStoreOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Store Suppliers'), ['controller' => 'UserStoreSuppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store Supplier'), ['controller' => 'UserStoreSuppliers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="suppliers view large-9 medium-8 columns content">
    <h3><?= h($supplier->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Supplier Name') ?></th>
            <td><?= h($supplier->supplier_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Email') ?></th>
            <td><?= h($supplier->supplier_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Phone No') ?></th>
            <td><?= h($supplier->supplier_phone_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Address') ?></th>
            <td><?= h($supplier->supplier_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($supplier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($supplier->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($supplier->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Price Checks') ?></h4>
        <?php if (!empty($supplier->price_checks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Date Entered') ?></th>
                <th scope="col"><?= __('Product Price') ?></th>
                <th scope="col"><?= __('PriceCheck Notes') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($supplier->price_checks as $priceChecks): ?>
            <tr>
                <td><?= h($priceChecks->id) ?></td>
                <td><?= h($priceChecks->user_store_id) ?></td>
                <td><?= h($priceChecks->supplier_id) ?></td>
                <td><?= h($priceChecks->product_id) ?></td>
                <td><?= h($priceChecks->date_entered) ?></td>
                <td><?= h($priceChecks->product_price) ?></td>
                <td><?= h($priceChecks->priceCheck_notes) ?></td>
                <td><?= h($priceChecks->created) ?></td>
                <td><?= h($priceChecks->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PriceChecks', 'action' => 'view', $priceChecks->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PriceChecks', 'action' => 'edit', $priceChecks->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PriceChecks', 'action' => 'delete', $priceChecks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $priceChecks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Store Orders') ?></h4>
        <?php if (!empty($supplier->user_store_orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Ordered Qty') ?></th>
                <th scope="col"><?= __('Order Date') ?></th>
                <th scope="col"><?= __('Same Day Order') ?></th>
                <th scope="col"><?= __('Order Notes') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($supplier->user_store_orders as $userStoreOrders): ?>
            <tr>
                <td><?= h($userStoreOrders->id) ?></td>
                <td><?= h($userStoreOrders->user_store_id) ?></td>
                <td><?= h($userStoreOrders->supplier_id) ?></td>
                <td><?= h($userStoreOrders->product_id) ?></td>
                <td><?= h($userStoreOrders->ordered_qty) ?></td>
                <td><?= h($userStoreOrders->order_date) ?></td>
                <td><?= h($userStoreOrders->same_day_order) ?></td>
                <td><?= h($userStoreOrders->order_notes) ?></td>
                <td><?= h($userStoreOrders->created) ?></td>
                <td><?= h($userStoreOrders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserStoreOrders', 'action' => 'view', $userStoreOrders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserStoreOrders', 'action' => 'edit', $userStoreOrders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserStoreOrders', 'action' => 'delete', $userStoreOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreOrders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Store Suppliers') ?></h4>
        <?php if (!empty($supplier->user_store_suppliers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($supplier->user_store_suppliers as $userStoreSuppliers): ?>
            <tr>
                <td><?= h($userStoreSuppliers->id) ?></td>
                <td><?= h($userStoreSuppliers->user_store_id) ?></td>
                <td><?= h($userStoreSuppliers->supplier_id) ?></td>
                <td><?= h($userStoreSuppliers->created) ?></td>
                <td><?= h($userStoreSuppliers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserStoreSuppliers', 'action' => 'view', $userStoreSuppliers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserStoreSuppliers', 'action' => 'edit', $userStoreSuppliers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserStoreSuppliers', 'action' => 'delete', $userStoreSuppliers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreSuppliers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
