<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Store Order'), ['action' => 'edit', $userStoreOrder->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Store Order'), ['action' => 'delete', $userStoreOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreOrder->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Store Orders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store Order'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userStoreOrders view large-9 medium-8 columns content">
    <h3><?= h($userStoreOrder->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Store') ?></th>
            <td><?= $userStoreOrder->has('user_store') ? $this->Html->link($userStoreOrder->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $userStoreOrder->user_store->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier') ?></th>
            <td><?= $userStoreOrder->has('supplier') ? $this->Html->link($userStoreOrder->supplier->id, ['controller' => 'Suppliers', 'action' => 'view', $userStoreOrder->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $userStoreOrder->has('product') ? $this->Html->link($userStoreOrder->product->id, ['controller' => 'Products', 'action' => 'view', $userStoreOrder->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Same Day Order') ?></th>
            <td><?= h($userStoreOrder->same_day_order) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userStoreOrder->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ordered Qty') ?></th>
            <td><?= $this->Number->format($userStoreOrder->ordered_qty) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Date') ?></th>
            <td><?= h($userStoreOrder->order_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userStoreOrder->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userStoreOrder->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Order Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($userStoreOrder->order_notes)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Invoices') ?></h4>
        <?php if (!empty($userStoreOrder->invoices)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Store Order Id') ?></th>
                <th scope="col"><?= __('Invoice Notes') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($userStoreOrder->invoices as $invoices): ?>
            <tr>
                <td><?= h($invoices->id) ?></td>
                <td><?= h($invoices->user_store_order_id) ?></td>
                <td><?= h($invoices->invoice_notes) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Invoices', 'action' => 'view', $invoices->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Invoices', 'action' => 'edit', $invoices->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Invoices', 'action' => 'delete', $invoices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invoices->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
