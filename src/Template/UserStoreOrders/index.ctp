<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="" id="otherActionNav">
    <ul class="nav nav-pills">
		<h4>Other Navigations:-</h4>
        <li><?= $this->Html->link(__('New User Store Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div id="indexContent" class="">
    <h3><?= __('User Store Orders') ?></h3>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_store_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ordered_qty') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('same_day_order') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userStoreOrders as $userStoreOrder): ?>
            <tr>
                <td><?= $this->Number->format($userStoreOrder->id) ?></td>
                <td><?= $userStoreOrder->has('user_store') ? $this->Html->link($userStoreOrder->user_store->id, ['controller' => 'UserStores', 'action' => 'view', $userStoreOrder->user_store->id]) : '' ?></td>
                <td><?= $userStoreOrder->has('supplier') ? $this->Html->link($userStoreOrder->supplier->supplier_name, ['controller' => 'Suppliers', 'action' => 'view', $userStoreOrder->supplier->id]) : '' ?></td>
                <td><?= $userStoreOrder->has('product') ? $this->Html->link($userStoreOrder->product->product_name, ['controller' => 'Products', 'action' => 'view', $userStoreOrder->product->id]) : '' ?></td>
                <td><?= $this->Number->format($userStoreOrder->ordered_qty) ?></td>
                <td><?= h($userStoreOrder->order_date) ?></td>
                <td><?= h($userStoreOrder->same_day_order) ?></td>
                <td><?= h($userStoreOrder->created) ?></td>
                <td><?= h($userStoreOrder->modified) ?></td>
                <td class="actions">
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-eye-open"></span>',['action' => 'view', $userStoreOrder->id],['class' => 'btn btn-primary', 'role' => 'button', 'escape' => false]);?>
					<?php echo $this->Html->link('<span class = "glyphicon glyphicon-pencil"></span>',['action' => 'edit', $userStoreOrder->id],['class' => 'btn btn-success', 'role' => 'button', 'escape' => false]);?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userStoreOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreOrder->id), 'class' => 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
