<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $userStoreOrder->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $userStoreOrder->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User Store Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List User Stores'), ['controller' => 'UserStores', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Store'), ['controller' => 'UserStores', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userStoreOrders form large-9 medium-8 columns content">
    <?= $this->Form->create($userStoreOrder) ?>
    <fieldset>
        <legend><?= __('Edit User Store Order') ?></legend>
        <?php
            echo $this->Form->control('user_store_id', ['options' => $userStores]);
            echo $this->Form->control('supplier_id', ['options' => $suppliers]);
            echo $this->Form->control('product_id', ['options' => $products]);
            echo $this->Form->control('ordered_qty');
            echo $this->Form->control('order_date');
            echo $this->Form->control('same_day_order');
            echo $this->Form->control('order_notes');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
