<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserStoreSuppliers Controller
 *
 * @property \App\Model\Table\UserStoreSuppliersTable $UserStoreSuppliers
 */
class UserStoreSuppliersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserStores', 'Suppliers']
        ];
        $userStoreSuppliers = $this->paginate($this->UserStoreSuppliers);

        $this->set(compact('userStoreSuppliers'));
        $this->set('_serialize', ['userStoreSuppliers']);
    }

    /**
     * View method
     *
     * @param string|null $id User Store Supplier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userStoreSupplier = $this->UserStoreSuppliers->get($id, [
            'contain' => ['UserStores', 'Suppliers']
        ]);

        $this->set('userStoreSupplier', $userStoreSupplier);
        $this->set('_serialize', ['userStoreSupplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userStoreSupplier = $this->UserStoreSuppliers->newEntity();
        if ($this->request->is('post')) {
            $userStoreSupplier = $this->UserStoreSuppliers->patchEntity($userStoreSupplier, $this->request->getData());
            if ($this->UserStoreSuppliers->save($userStoreSupplier)) {
                $this->Flash->success(__('The user store supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user store supplier could not be saved. Please, try again.'));
        }
        $userStores = $this->UserStoreSuppliers->UserStores->find('list', ['limit' => 200]);
        $suppliers = $this->UserStoreSuppliers->Suppliers->find('list', ['limit' => 200]);
        $this->set(compact('userStoreSupplier', 'userStores', 'suppliers'));
        $this->set('_serialize', ['userStoreSupplier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Store Supplier id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userStoreSupplier = $this->UserStoreSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userStoreSupplier = $this->UserStoreSuppliers->patchEntity($userStoreSupplier, $this->request->getData());
            if ($this->UserStoreSuppliers->save($userStoreSupplier)) {
                $this->Flash->success(__('The user store supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user store supplier could not be saved. Please, try again.'));
        }
        $userStores = $this->UserStoreSuppliers->UserStores->find('list', ['limit' => 200]);
        $suppliers = $this->UserStoreSuppliers->Suppliers->find('list', ['limit' => 200]);
        $this->set(compact('userStoreSupplier', 'userStores', 'suppliers'));
        $this->set('_serialize', ['userStoreSupplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Store Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userStoreSupplier = $this->UserStoreSuppliers->get($id);
        if ($this->UserStoreSuppliers->delete($userStoreSupplier)) {
            $this->Flash->success(__('The user store supplier has been deleted.'));
        } else {
            $this->Flash->error(__('The user store supplier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
