<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PriceChecks Controller
 *
 * @property \App\Model\Table\PriceChecksTable $PriceChecks
 */
class PriceChecksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserStores', 'Suppliers', 'Products']
        ];
        $priceChecks = $this->paginate($this->PriceChecks);

        $this->set(compact('priceChecks'));
        $this->set('_serialize', ['priceChecks']);
    }

    /**
     * View method
     *
     * @param string|null $id Price Check id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $priceCheck = $this->PriceChecks->get($id, [
            'contain' => ['UserStores', 'Suppliers', 'Products']
        ]);

        $this->set('priceCheck', $priceCheck);
        $this->set('_serialize', ['priceCheck']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $priceCheck = $this->PriceChecks->newEntity();
        if ($this->request->is('post')) {
            $priceCheck = $this->PriceChecks->patchEntity($priceCheck, $this->request->getData());
            if ($this->PriceChecks->save($priceCheck)) {
                $this->Flash->success(__('The price check has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The price check could not be saved. Please, try again.'));
        }
        $userStores = $this->PriceChecks->UserStores->find('list')->contain(
																	[
																		'Users' => ['fields' => ['user_name']],
																		'Stores' => ['fields' => ['store_name']]
																	]);
        $suppliers = $this->PriceChecks->Suppliers->find('list', ['limit' => 200]);
        $products = $this->PriceChecks->Products->find('list', ['limit' => 200]);
        $this->set(compact('priceCheck', 'userStores', 'suppliers', 'products'));
        $this->set('_serialize', ['priceCheck']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Price Check id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $priceCheck = $this->PriceChecks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $priceCheck = $this->PriceChecks->patchEntity($priceCheck, $this->request->getData());
            if ($this->PriceChecks->save($priceCheck)) {
                $this->Flash->success(__('The price check has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The price check could not be saved. Please, try again.'));
        }
        $userStores = $this->PriceChecks->UserStores->find('list', ['limit' => 200]);
        $suppliers = $this->PriceChecks->Suppliers->find('list', ['limit' => 200]);
        $products = $this->PriceChecks->Products->find('list', ['limit' => 200]);
        $this->set(compact('priceCheck', 'userStores', 'suppliers', 'products'));
        $this->set('_serialize', ['priceCheck']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Price Check id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $priceCheck = $this->PriceChecks->get($id);
        if ($this->PriceChecks->delete($priceCheck)) {
            $this->Flash->success(__('The price check has been deleted.'));
        } else {
            $this->Flash->error(__('The price check could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
