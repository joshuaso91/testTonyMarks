<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserStoreOrders Controller
 *
 * @property \App\Model\Table\UserStoreOrdersTable $UserStoreOrders
 */
class UserStoreOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserStores', 'Suppliers', 'Products']
        ];
        $userStoreOrders = $this->paginate($this->UserStoreOrders);

        $this->set(compact('userStoreOrders'));
        $this->set('_serialize', ['userStoreOrders']);
    }
	
    /**
     * View method
     *
     * @param string|null $id User Store Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userStoreOrder = $this->UserStoreOrders->get($id, [
            'contain' => ['UserStores', 'Suppliers', 'Products', 'Invoices']
        ]);

        $this->set('userStoreOrder', $userStoreOrder);
        $this->set('_serialize', ['userStoreOrder']);
    }
	
	
    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userStoreOrder = $this->UserStoreOrders->newEntity();
        if ($this->request->is('post')) {
            $userStoreOrder = $this->UserStoreOrders->patchEntity($userStoreOrder, $this->request->getData());
            if ($this->UserStoreOrders->save($userStoreOrder)) {
                $this->Flash->success(__('The user store order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user store order could not be saved. Please, try again.'));
        }
        $userStores = $this->UserStoreOrders->UserStores->find('list');
        $suppliers = $this->UserStoreOrders->Suppliers->find('list');
        $products = $this->UserStoreOrders->Products->find('list');
        $this->set(compact('userStoreOrder', 'userStores', 'suppliers', 'products'));
        $this->set('_serialize', ['userStoreOrder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User Store Order id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userStoreOrder = $this->UserStoreOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userStoreOrder = $this->UserStoreOrders->patchEntity($userStoreOrder, $this->request->getData());
            if ($this->UserStoreOrders->save($userStoreOrder)) {
                $this->Flash->success(__('The user store order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user store order could not be saved. Please, try again.'));
        }
        $userStores = $this->UserStoreOrders->UserStores->find('list');
        $suppliers = $this->UserStoreOrders->Suppliers->find('list');
        $products = $this->UserStoreOrders->Products->find('list');
        $this->set(compact('userStoreOrder', 'userStores', 'suppliers', 'products'));
        $this->set('_serialize', ['userStoreOrder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User Store Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userStoreOrder = $this->UserStoreOrders->get($id);
        if ($this->UserStoreOrders->delete($userStoreOrder)) {
            $this->Flash->success(__('The user store order has been deleted.'));
        } else {
            $this->Flash->error(__('The user store order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
