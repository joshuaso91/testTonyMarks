<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Stocktakes Controller
 *
 * @property \App\Model\Table\StocktakesTable $Stocktakes
 */
class StocktakesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['UserStores' => function($query)
                          {
                              return $query->find('userStoreName');
                          }, 
                          'Products']
        ];
        $stocktakes = $this->paginate($this->Stocktakes); //`$this->paginate()` can take a Table instance *or* a query
        $this->set(compact('stocktakes'));
        $this->set('_serialize', ['stocktakes']);
    }

    /**
     * View method
     *
     * @param string|null $id Stocktake id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $stocktake = $this->Stocktakes->get($id, [
            'contain' => ['UserStores', 'Products']
        ]);

        $this->set('stocktake', $stocktake);
        $this->set('_serialize', ['stocktake']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $stocktake = $this->Stocktakes->newEntity();
        if ($this->request->is('post')) {
            $stocktake = $this->Stocktakes->patchEntity($stocktake, $this->request->getData());
            if ($this->Stocktakes->save($stocktake)) {
                $this->Flash->success(__('The stocktake has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
        }
        $userStores = $this->Stocktakes->UserStores->find('list')->contain(
																	[
																		'Users' => ['fields' => ['user_name']],
																		'Stores' => ['fields' => ['store_name']]
																	]);
        $products = $this->Stocktakes->Products->find('list');
        $this->set(compact('stocktake', 'userStores', 'products'));
        $this->set('_serialize', ['stocktake']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Stocktake id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $stocktake = $this->Stocktakes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $stocktake = $this->Stocktakes->patchEntity($stocktake, $this->request->getData());
            if ($this->Stocktakes->save($stocktake)) {
                $this->Flash->success(__('The stocktake has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
        }
         $userStores = $this->Stocktakes->UserStores->find('list')->contain(
																	[
																		'Users' => ['fields' => ['user_name']],
																		'Stores' => ['fields' => ['store_name']]
																	]);
        $products = $this->Stocktakes->Products->find('list');
        $this->set(compact('stocktake', 'userStores', 'products'));
        $this->set('_serialize', ['stocktake']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Stocktake id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $stocktake = $this->Stocktakes->get($id);
        if ($this->Stocktakes->delete($stocktake)) {
            $this->Flash->success(__('The stocktake has been deleted.'));
        } else {
            $this->Flash->error(__('The stocktake could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
